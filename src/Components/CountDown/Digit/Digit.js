import React from 'react'
import './Digit.css'

const Digit = (props) => {
    return (
        <div className="digit mx-auto">
            <h1 className="pt-5 mt-3">
                {props.value<10 ? '0' : ''}{props.value}
            </h1>
        </div>
    )
};
export default Digit