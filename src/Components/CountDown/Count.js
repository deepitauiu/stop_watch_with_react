import React from 'react'
import Digit from "./Digit/Digit";

const CountDown = (props) => {
    return (
        <div className="d-flex text-center my-5">
            <Digit className="text-success mx-auto" value={props.time.min} />
            <Digit className="text-warning mx-auto" value={props.time.sec} />
            <Digit className="text-danger mx-auto" value={props.time.milli}/>
        </div>
    )
};
export default CountDown