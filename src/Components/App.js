import React, { Component } from 'react';
import './App.css';
import Title from "./Title/Title";
import CountDown from "./CountDown/Count";
import Controller from "./Controller/Controller";
import Laps from "./laps/laps";

class App extends Component {
    constructor(props){
        super(props);
        this.state={
            time:{
                min:0,
                sec:0,
                milli:0
            },
            laps:[]
        };
    }
    getStart(){
        this.intervalId=setInterval(()=>{
            let min=this.state.time.min;
            let sec=this.state.time.sec;
            let milli=this.state.time.milli;
            if(milli>=10){
                sec=sec+1;
                milli=0;
            }
            if(sec>=59)
            {
                min+=1
                sec=0
            }
            this.setState({
                time:{
                    ...this.state,
                    min:min,
                    sec:sec,
                    milli:milli+1
                },
            });
        },100);
    }
    getPause(){
        clearInterval(this.intervalId);
    }
    getReset(){
        clearInterval(this.intervalId);
        this.setState({
            time:{
                ...this.state,
                min:0,
                sec:0,
                milli:0
            },
            laps:[]
        });
    }
    getlap(){
        let time = {
            ...this.state.time
        }
        this.setState({
            ...this.state,
            laps: [time, ...this.state.laps]
        })

        console.log(this.state.laps)
    }

  render() {
    return (
      <div className="App">
        <div className="container py-5">
          <div className="display-4 text-success">
              {/*<h2> Stop Watch</h2>*/}
              <div className="row">
                  <div className="col-sm-8 offset-sm-2">
                        <Title/>
                        <CountDown time={this.state.time}/>
                  </div>
                  <div className="col-sm-8 offset-sm-2">
                        <Controller
                         start={this.getStart.bind(this)}
                         pause={this.getPause.bind(this)}
                         reset={this.getReset.bind(this)}
                         lap={this.getlap.bind(this)}
                        />
                  </div>
                  <div className="col-sm-8 offset-sm-2">
                      <Laps laps={this.state.laps}/>
                  </div>
              </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
