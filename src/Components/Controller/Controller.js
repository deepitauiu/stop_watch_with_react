import React, {Component} from 'react';

class Controller extends Component {
    constructor(props) {
        super(props);
        this.state = {
            start: true,
            lap: false,
            pause: false,
            reset: false
        }
    }
    actionHandler(v){
        if(v==='start')
        {
            this.setState(
                {
                    start:false,
                    lap: false,
                }
            );
            this.props.start();
        }
       else if(v==='pause')
        {
            this.setState(
                {
                    start:true,
                    reset:true,
                    lap: false,
                }
            );
            this.props.pause();
        }
        else if(v==='reset')
        {
            this.setState(
                {
                    start:true,
                    reset:false,
                    lap: false,
                }
            );
            this.props.reset();
        }
       else if(v==='lap')
        {
            this.props.lap();
        }

    }

    getController() {
        let output = null;
        if (this.state.start && !this.state.reset) {
            output = (
                <div>
                    <button className='btn-sm btn-success px-5'
                            onClick={(v)=>this.actionHandler('start')}>
                        Start
                    </button>
                </div>
            )
        }
        else if (!this.state.start) {
            output = (
                <div>
                    <button className='btn-sm btn-warning px-5 mx-auto' onClick={(v)=>this.actionHandler('pause')}>Pause</button>
                    <button className='btn-sm btn-primary px-5 ml-5' onClick={(v)=>this.actionHandler('lap')}> Lap</button>
                </div>
            )
        }
        else if (this.state.start && this.state.reset) {
            output = (
                <div>
                    <button className='btn-sm btn-success px-5'
                            onClick={(v)=>this.actionHandler('start')}>
                        Start
                    </button>
                    <button className='btn-sm btn-danger px-5 ml-5'
                            onClick={(v)=>this.actionHandler('reset')}>
                        Reset
                    </button>
                </div>
            )
        }

        return output;
    }

    render() {
        return this.getController();
    }

}

export default Controller