import React from 'react'
import './Title.css'

class Title extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "Stop watch",
            isInput: false
        }
    }

    inputHandler() {
        this.setState(
            {
                ...this.state,
                isInput: true
            }
        )
    }
    titleHandler(e){
        this.setState(
            {
                ...this.state,
                title:e.target.value
            }
        );
        console.log(e.target.value)
    }
    actionHandler(e){
        if(e.key==='Enter')
        {
            this.setState(
                {
                    ...this.state,
                    isInput: false
                }
            )
        }
    }

    render() {
        let output = null;
        if (this.state.isInput) {
            output=(
                <div className="Title">
                    <input className="form-control"
                           type="text" value={this.state.title}
                           onChange={(e) => this.titleHandler(e)}
                           onKeyPress={(e)=>this.actionHandler(e)}
                    />
                </div>
            )
        }
        else {
            output = (
                <div className="d-flex">
                    <h1 className="display-4 Title">{this.state.title}</h1>
                    <span className="display-1 ml-auto icon ">
                        <i className="fas fa-pencil-alt"
                           onClick={() => this.inputHandler()}>
                        </i>
                    </span>
                </div>
            )
        }
        return (
            <div>
                {output}
            </div>
        )
    }
}

export default Title